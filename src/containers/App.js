import React, { Component } from 'react';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import Colors from '../components/Colors';
import Countries from '../components/Countries';
import Cities from '../components/Cities';

import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Colors />
        <Countries />
        <Cities countryId={this.props.countryId} />
      </div>
    );
  }
}

const selector = formValueSelector('countries');
const mapStateToProps = (state) => {
  const countryId = selector(state, 'country');
  return { countryId };
};

App = connect(mapStateToProps)(App);

export default App;

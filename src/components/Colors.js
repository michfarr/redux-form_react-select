import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';
import SelectForm from './Form';

import colors from './colors.json';

const ReduxSelect = reduxForm({
  form: 'colors'
})(SelectForm);

class ColorSelect extends PureComponent {

  defaultValue() {
    return colors[0]
  }

  render() {
    return (
      <ReduxSelect
        name="color"
        options={colors}
        valueKey="value"
        labelKey="label"
        defaultValue={this.defaultValue()}
      />
    );
  }
}

export default ColorSelect;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import BaseSelect from './Select';

import './styles.css';

class SelectForm extends PureComponent {

  static propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    valueKey: PropTypes.string.isRequired,
    labelKey: PropTypes.string.isRequired,
    handleSubmit: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { name, defaultValue, initialize, valueKey } = this.props

    if (!!defaultValue) {
      initialize({ [name]: defaultValue[valueKey] })
    }
  }

  componentDidUpdate(prevProps) {
    const { name, defaultValue, initialize, valueKey } = this.props

    if (!!defaultValue && (prevProps.defaultValue !== defaultValue)) {
      initialize({ [name]: defaultValue[valueKey] })
    }
  }

  onFormSubmit(formData) {
    // console.log(FormData);
  }

  render() {
    const {
      name,
      options,
      valueKey,
      labelKey,
      handleSubmit
    } = this.props

    return (
      <form
        className="RSelect-form"
        onSubmit={this.onFormSubmit.bind(this)}
      >
        <Field
          name={name}
          component={BaseSelect}
          options={options}
          valueKey={valueKey}
          labelKey={labelKey}
        />
      </form>
    );
  }
}

export default SelectForm;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

import arrowDown from '../../assets/arrow-down.svg';
import './override.css';
import './styles.css';

class BaseSelect extends PureComponent {

  static propTypes = {
    input: PropTypes.shape({
      name: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ]).isRequired,
      onBlur: PropTypes.func.isRequired,
      onChange: PropTypes.func.isRequired,
      onFocus: PropTypes.func.isRequired,
    }).isRequired,
    options: PropTypes.array.isRequired,
    valueKey: PropTypes.string.isRequired,
    labelKey: PropTypes.string.isRequired
  };

  onChange(event) {
    const { input, valueKey } = this.props;

    if (input.onChange && event != null) {
      input.onChange(event[valueKey]);
    } else {
      input.onChange(null);
    }
  }

  renderArrow() {
    return (
      <div className="RSelect-arrow">
        <span>| </span>
        <img src={arrowDown} alt="arrow" />
      </div>
    );
  }

  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  render() {
    const { input } = this.props

    return (
      <div>
        <p className="RSelect-heading">{ this.capitalize(input.name) }</p>
        <Select
          value={input.value || ''}
          onBlur={() => input.onBlur(input.value)}
          onChange={this.onChange.bind(this)}
          arrowRenderer={this.renderArrow}
          { ...this.props }
        />
      </div>
    );
  }
}

export default BaseSelect;

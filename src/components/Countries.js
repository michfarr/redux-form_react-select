import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';
import SelectForm from './Form';

const COUNTRIES_URL = 'http://kxhipq.codehut.io/api/country/';
const ReduxSelect = reduxForm({
  form: 'countries'
})(SelectForm);

class CountriesSelect extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      countries: [],
      defaultValue: void(0)
    };
  }

  componentDidMount() {
    this.getCountries();
  }

  getCountries() {
    fetch(COUNTRIES_URL, { method: 'POST' })
      .then(res => res.json())
      .then(countries => this.setState({
        countries,
        defaultValue: countries[0]
      }));
  }

  render() {
    return (
      <ReduxSelect
        name="country"
        options={this.state.countries}
        valueKey="id"
        labelKey="name"
        defaultValue={this.state.defaultValue}
      />
    );
  }
}

export default CountriesSelect;

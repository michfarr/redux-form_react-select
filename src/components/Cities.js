import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import SelectForm from './Form';

const CITIES_URL = 'http://kxhipq.codehut.io/api/city/';
const ReduxSelect = reduxForm({
  form: 'cities'
})(SelectForm);

class CitiesSelect extends PureComponent {

  static propTypes = {
    countryId: PropTypes.number
  }

  constructor(props) {
    super(props);

    this.state = {
      cities: [],
      defaultValue: void(0)
    };
  }

  componentDidMount() {
    this.getCities(this.props.countryId);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.countryId !== this.props.countryId) {
      this.getCities(this.props.countryId);
    }
  }

  getCities(country_id) {
    if (!country_id) return;

    fetch(CITIES_URL, {
      method: 'POST',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify({ country_id })
    })
      .then(res => res.json())
      .then(cities => this.setState({
        cities,
        defaultValue: cities[0]
      }));
  }

  render() {
    return (
      <ReduxSelect
        name="city"
        options={this.state.cities}
        valueKey="id"
        labelKey="name"
        defaultValue={this.state.defaultValue}
      />
    );
  }
}

export default CitiesSelect;

Setup
===

```
$ git clone git@gitlab.com:michfarr/redux-form_react-select
$ cd redux-form_react-select
$ yarn install && yarn start
```

## Project

I only created a store for the form, as that is what was described in the job
details.  I performed all API calls an JSON loading in the Selector components
(`./components/{Colors,Countries,Cities}.js`) and passed the results as options
for the Form component (`./components/Form/index.js`).

All of the Redux Form setup is actually done in the Selector components.

I originally tried using the Select.Async component from React-Select for the
API calls, but apparently it prefers to filter the contents on the client side
rather than at the server side.  After a few varied failures I decided I would
just pass the results of the API call to options from a parent and use the
redux form reducer to pass changes for the country_id to the Cities selector.

## Structure

All components are in `/src/components`
The `App.js` was moved to `/src/containers`
Beyond that, I only added the reducer for `redux-form` in `/src/reducers/index.js`
